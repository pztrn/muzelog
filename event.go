// Copyright (c) 2018, Stanislav N. aka pztrn.
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject
// to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package muzelog

import (
	// stdlib
	"fmt"
	"runtime"
	"time"
)

// Event represents single log line. This structure will be created on
// Debug/Info/Warn/Error/Fatal calls and distributed among all loggers.
// This structure has some unexported fields that is neccessary for
// logging, therefore protected. Please use proper setter or getter!
type Event struct {
	level   LogLevel
	message string
	time    time.Time
	trace   []byte
}

// FormatAsString formats message as simple string.
func (e *Event) FormatAsString() string {
	return e.time.Format("2006-01-02 15:04:05") + "|" + e.level.String() + "| " + e.message
}

// Level returns numeric (uint8) representation of log level for
// this event. It can be compared to muzelog.LogLevel.
func (e *Event) Level() LogLevel {
	return e.level
}

// LevelAsString returns stringified representation of log level
// for this event.
func (e *Event) LevelAsString() string {
	return e.Level().String()
}

// Message returns message for event.
func (e *Event) Message() string {
	return e.message
}

// SetLevel sets log level for this event.
func (e *Event) SetLevel(lvl LogLevel) {
	e.level = lvl
}

// SetMessage sets message for this event.
func (e *Event) SetMessage(msg string) {
	e.message = msg
}

// SetTimestamp sets timestamp for log event.
func (e *Event) SetTimestamp() {
	e.time = time.Now().UTC()
}

// SetTrace sets traceback for current event.
func (e *Event) SetTrace() {
	runtime.Stack(e.trace, false)
	fmt.Println(e.trace)
}

// Timestamp returns timestamp for this log event.
func (e *Event) Timestamp() time.Time {
	return e.time
}

// Trace formats trace and returns as string.
func (e *Event) Trace() string {
	return string(e.trace)
}
