[![GoDoc](https://godoc.org/gitlab.com/pztrn/muzelog?status.svg)](https://godoc.org/gitlab.com/pztrn/muzelog)

# MuzeLog

**MuzeLog** is a logging solution for Go applications written with performance and ease-of-use in mind. It is able to be used as singleton or as structure for application-wide contexts.

The ultimate goal for this package is to provide AIO logging solution without hassling with searching for hooks, third-party packages and etc. for logging to ``put_your_log_aggregation_solution_here``. This also means that users who wants to lower binary size might not find MuzeLog suitable solution for them.

## Why MuzeLog was created?

First - a performance bottleneck. Most loggers do it's job in synchronous manner to ensure that log lines are flushed to writer, which can be a performance bottleneck in some cases. MuzeLog is completely asynchronous by default yet (with proper code) can also ensure that messages are flushed to output.

Second - hooks approach sometimes might cause misunderstanding about ways it should be used or ways it really works. MuzeLog is about to stop that - there is no hooks, but outputs, that must comply with io.Writer interface. As simple as that.

## Abilities

Currently MuzeLog is able to log:

1. In stdout.
2. In memory with possibility to subscribe to logging events and obtain all logged data from memory.

## How it works

After issuing logging method it'll prepare log line. This logline will be added to queue for processing.

Separate goroutine is responsible for log lines distributing between outputs using channels. Every output (which is a MuzeLogger instance) creates own channel for receiving log lines sent by distributing goroutine.

Every output also has separate goroutine and queue for sending messages to endpoints.

In short, scheme is:

```
MASTER GOROUTINE       | Prepares log line, adds to master queue which is
            |          | processed by  distributing goroutine. It will send 
            |          | generated log lines to distributing queue with a channel.
            |
DISTRIBUTING QUEUE     | A slice for all generated log lines. Filled by distributing goroutine.
            |
DISTRIBUTING GOROUTINE | Distributes log lines from master queue to outputs using channels.
            |          | After sending it will clear distributing queue.
            |          | While working distributed queue will be locked, so all new log
            |          | lines will be stuck in receiving channel for a while.
            |
OUTPUT QUEUE           | A slice belonging to every output created which, as distributing
                       | queue, contains a copy of log line which should be sent to output.
            |
OUTPUT GOROUTINE       | Sends received log lines to output (stdout, sentry, etc.)
            |
OUTPUT                 | Shows, records, stores received log line.
```

This scheme, of course, adds some limitations:

* Application **SHOULD** shut down properly to ensure that log lines wasn't lost

## Examples

Take a look at examples directory for run-ready examples and quickstarts.