package main

import (
	// stdlib
	"fmt"
	"os"
	"time"

	// other
	"gitlab.com/pztrn/muzelog"
)

func main() {
	muzelog.CreateOutput("stdout2", os.Stdout)

	fmt.Println("Default log level - INFO")
	testOutput()

	fmt.Println("Set default level to ERROR")
	muzelog.SetLogLevel(muzelog.ErrorLevel)
	testOutput()

	fmt.Println("Set stdout log level to INFO")
	muzelog.SetLogLevelForOutput("stdout", muzelog.InfoLevel)
	testOutput()

	fmt.Println("Shutting down loggers...")
	muzelog.Shutdown()
	fmt.Println("Loggers shutdown")
}

func testOutput() {
	muzelog.Debug("Stdout test")
	time.Sleep(time.Millisecond * 100)
	muzelog.Info("Stdout test")
	time.Sleep(time.Millisecond * 100)
	muzelog.Warn("Stdout test")
	time.Sleep(time.Millisecond * 100)
	muzelog.Error("Stdout test")
	time.Sleep(time.Millisecond * 100)
	muzelog.Fatal("Stdout test")
	time.Sleep(time.Millisecond * 100)
}
