// Copyright (c) 2018, Stanislav N. aka pztrn.
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject
// to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package muzelog

import (
	// stdlib
	"fmt"
	"io"
)

var log *MuzeLog

// Creates basic logger which is used everywhere.
func init() {
	log = NewMuzeLogWithStdout()
}

// CreateOutput creates output for basic logger.
func CreateOutput(name string, output io.Writer) {
	log.CreateOutput(name, output)
}

// Debug creates DEBUG level event.
func Debug(msg string) {
	log.Debug(msg)
}

// Debugf creates DEBUG level event and formats it with fmt.Sprintf.
func Debugf(format string, args ...interface{}) {
	line := fmt.Sprintf(format, args...)
	log.Debug(line)
}

// Error creates ERROR level event.
func Error(msg string) {
	log.Error(msg)
}

// Errorf creates ERROR level event and formats it with fmt.Sprintf.
func Errorf(format string, args ...interface{}) {
	line := fmt.Sprintf(format, args...)
	log.Error(line)
}

// Fatal creates FATAL level event.
func Fatal(msg string) {
	log.Fatal(msg)
}

// Fatalf creates FATAL level event and formats it with fmt.Sprintf.
func Fatalf(format string, args ...interface{}) {
	line := fmt.Sprintf(format, args...)
	log.Fatal(line)
}

// Info creates INFO level event.
func Info(msg string) {
	log.Info(msg)
}

// Infof creates INFO level event and formats it with fmt.Sprintf.
func Infof(format string, args ...interface{}) {
	line := fmt.Sprintf(format, args...)
	log.Debug(line)
}

// SetLogLevel sets logging level for all outputs for basic logger.
func SetLogLevel(level LogLevel) {
	SetLogLevelForOutput("*", level)
}

// SetLogLevelForOutput sets logging level for specified output from
// basic logger.
func SetLogLevelForOutput(outputName string, level LogLevel) {
	log.SetLogLevel(outputName, level)
}

// Shutdown shutdowns loggers.
func Shutdown() {
	log.Shutdown()
}

// Warn creates WARN level event.
func Warn(msg string) {
	log.Warn(msg)
}

// Warnf creates WARN level event and formats it with fmt.Sprintf.
func Warnf(format string, args ...interface{}) {
	line := fmt.Sprintf(format, args...)
	log.Warn(line)
}
