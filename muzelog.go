// Copyright (c) 2018, Stanislav N. aka pztrn.
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject
// to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package muzelog

import (
	// stdlib
	"io"
	"os"
	"sync"
	"time"
)

const (
	// Default log level.
	defaultLogLevel = DebugLevel
)

// NewMuzeLog creates new MuzeLog instance.
func NewMuzeLog() *MuzeLog {
	l := &MuzeLog{}
	l.Start()
	return l
}

// NewMuzeLogWithStdout creates new MuzeLog instance with stdout output.
func NewMuzeLogWithStdout() *MuzeLog {
	l := NewMuzeLog()
	l.CreateOutput("stdout", os.Stdout)
	l.SetLogLevel("stdout", defaultLogLevel)

	return l
}

// MuzeLog is a main logging structure.
type MuzeLog struct {
	// List of initialized loggers.
	loggers      map[string]*MuzeLogger
	loggersMutex sync.Mutex

	// Channel for adding messages to distributing queue.
	distributingQueueChannel chan Event

	// Distributing queue.
	distributingQueue      []Event
	distributingQueueMutex sync.Mutex
}

func (ml *MuzeLog) createEvent(level LogLevel, msg string) Event {
	e := Event{}
	e.SetLevel(level)
	e.SetMessage(msg)
	e.SetTimestamp()
	return e
}

func (ml *MuzeLog) CreateOutput(name string, output io.Writer) {
	logger := NewLoggerWithOutput(name, output)
	ml.loggers[name] = logger
}

// Debug generates DEBUG level message and add it to distribution queue.
func (ml *MuzeLog) Debug(msg string) {
	e := ml.createEvent(DebugLevel, msg)
	ml.distributingQueueChannel <- e
}

// Error generates ERROR level message and add it to distribution queue.
func (ml *MuzeLog) Error(msg string) {
	e := ml.createEvent(ErrorLevel, msg)
	e.SetTrace()
	ml.distributingQueueChannel <- e
}

// Fatal generates FATAL level message and add it to distribution queue.
func (ml *MuzeLog) Fatal(msg string) {
	e := ml.createEvent(FatalLevel, msg)
	e.SetTrace()
	ml.distributingQueueChannel <- e
}

// Distributes messages between all loggers.
// Should be launched in separate goroutine.
func (ml *MuzeLog) distributor() {
	// ToDo: configurable timeout.
	t := time.NewTicker(time.Millisecond * 10)
	for range t.C {
		ml.distributingQueueMutex.Lock()
		if len(ml.distributingQueue) == 0 {
			// Queue is empty, do nothing.
			ml.distributingQueueMutex.Unlock()
			continue
		}
		// Queue isn't empty - copy it to internally used slice,
		// clean queue and release mutex so next messages can be added.
		queue := make([]Event, 0, len(ml.distributingQueue))
		for _, e := range ml.distributingQueue {
			queue = append(queue, e)
		}
		ml.distributingQueue = []Event{}
		ml.distributingQueueMutex.Unlock()

		// Start messages distributing.
		ml.loggersMutex.Lock()
		for _, event := range queue {
			for _, logger := range ml.loggers {
				logger.Process(event)
			}
		}
		ml.loggersMutex.Unlock()
	}
}

// Info generates INFO level message and adds it to distribution queue.
func (ml *MuzeLog) Info(msg string) {
	e := ml.createEvent(InfoLevel, msg)
	ml.distributingQueueChannel <- e
}

// Registers message in distributing queue.
// Should be launched in separate goroutine.
func (ml *MuzeLog) registrator() {
	for {
		select {
		case msg := <-ml.distributingQueueChannel:
			ml.distributingQueueMutex.Lock()
			ml.distributingQueue = append(ml.distributingQueue, msg)
			ml.distributingQueueMutex.Unlock()
		}
	}
}

// SetLogLevel sets default log level for passed output's name.
// If "*" was passed as output name - function will set log level
// for all loggers.
func (ml *MuzeLog) SetLogLevel(outputName string, level LogLevel) {
	ml.loggersMutex.Lock()
	defer ml.loggersMutex.Unlock()

	if outputName == "*" {
		for _, output := range ml.loggers {
			output.SetLogLevel(level)
		}
	} else {
		output, outputFound := ml.loggers[outputName]
		if outputFound {
			output.SetLogLevel(level)
		}
	}
}

// Shutdown shutdowns loggers and waits for them to complete their
// work. It will block caller's goroutine until all workers will
// done current queue.
func (ml *MuzeLog) Shutdown() {
	// Send shutdown event. After that event loggers won't process
	// anything else.
	e := Event{}
	e.SetLevel(ShutdownLevel)
	e.SetMessage("SHUTDOWN_REQUESTED")
	ml.distributingQueueChannel <- e

	// Wait for loggers to shut down.
	for {
		stillWorking := false
		ml.loggersMutex.Lock()
		for _, logger := range ml.loggers {
			if !logger.IsShuttedDown() {
				stillWorking = true
			}
		}
		ml.loggersMutex.Unlock()

		if !stillWorking {
			break
		}

		time.Sleep(time.Millisecond * 10)
	}
}

// Start starts distributing goroutine.
func (ml *MuzeLog) Start() {
	// Initialize storages and channels.
	ml.loggers = make(map[string]*MuzeLogger)
	ml.distributingQueueChannel = make(chan Event, 256)
	ml.distributingQueue = []Event{}

	go ml.registrator()
	go ml.distributor()
}

// Warn generates WARNING level message and add it to distribution queue.
func (ml *MuzeLog) Warn(msg string) {
	e := ml.createEvent(WarnLevel, msg)
	ml.distributingQueueChannel <- e
}
