// Copyright (c) 2018, Stanislav N. aka pztrn.
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject
// to the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
// OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package muzelog

import (
	// stdlib
	"io"
	"sync"
)

// NewLoggerWithOutput creates new logger with specified output.
func NewLoggerWithOutput(name string, output io.Writer) *MuzeLogger {
	mlg := &MuzeLogger{}
	mlg.Initialize()
	mlg.SetName(name)
	mlg.SetOutput(output)
	go mlg.StartWorking()
	return mlg
}

// MuzeLogger represents output logger structure.
type MuzeLogger struct {
	// Are we completed writing messages in queue?
	isShuttedDown bool

	// Bools aren't goroutine-safe :)
	isShuttedDownMutex sync.Mutex

	// Output's log level to use.
	level LogLevel
	// Output's name
	name string
	// Output where we're writing logging data.
	output io.Writer
	// Queue with received messages.
	queue chan Event
}

// Initialize initializes internal queue storage and some flags. Shouldn't
// be called manually.
func (mlg *MuzeLogger) Initialize() {
	mlg.isShuttedDown = false
	mlg.queue = make(chan Event, 4096)
}

// IsShuttedDown returns boolean value for flag that indicates if this
// output still works or already completed processing all events.
func (mlg *MuzeLogger) IsShuttedDown() bool {
	mlg.isShuttedDownMutex.Lock()
	defer mlg.isShuttedDownMutex.Unlock()
	return mlg.isShuttedDown
}

// Process sends passed event to output's processing queue.
func (mlg *MuzeLogger) Process(event Event) {
	mlg.queue <- event
}

// SetLogLevel sets log level for current output.
func (mlg *MuzeLogger) SetLogLevel(level LogLevel) {
	mlg.level = level
}

// SetName sets output name.
func (mlg *MuzeLogger) SetName(name string) {
	mlg.name = name
}

// SetOutput sets output. It must conform to io.Writer specification.
func (mlg *MuzeLogger) SetOutput(output io.Writer) {
	mlg.output = output
}

// StartWorking processes output queue. Should be started in separate
// goroutine to ensure async working. Should not be called manually!
func (mlg *MuzeLogger) StartWorking() {
	for {
		select {
		case event := <-mlg.queue:
			if event.Message() != "" {
				if event.Level() == ShutdownLevel && event.Message() == "SHUTDOWN_REQUESTED" {
					mlg.isShuttedDownMutex.Lock()
					defer mlg.isShuttedDownMutex.Unlock()
					mlg.isShuttedDown = true
					return
				}
				// Check if we should send data to output.
				if event.Level() >= mlg.level {
					formatted := event.FormatAsString()
					if event.Level() >= ErrorLevel {
						formatted += event.Trace()
					}
					mlg.output.Write([]byte("[" + mlg.name + "] " + formatted + "\n"))
				}
			}
		}
	}
}
